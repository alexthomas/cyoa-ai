FROM openjdk:21-slim-bullseye
EXPOSE 8080
COPY build/libs/cyoa-ai.jar cyoa-ai.jar

ENTRYPOINT java -jar cyoa-ai.jar -Xmx128m