package com.alexk8s.cyoaai;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.r2dbc.config.EnableR2dbcAuditing;

@SpringBootApplication(exclude = {org.springframework.ai.autoconfigure.openai.OpenAiAutoConfiguration.class})
@EnableR2dbcAuditing
public class CyoaAiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CyoaAiApplication.class, args);
	}

}
