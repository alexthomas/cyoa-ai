package com.alexk8s.cyoaai.util;

import com.alexk8s.cyoaai.model.Usage;
import com.alexk8s.cyoaai.repository.UsageRepository;
import com.alexk8s.cyoaai.service.OpenAiService;
import lombok.RequiredArgsConstructor;
import org.springframework.ai.openai.api.OpenAiApi;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
public class UsageContext {
    private final List<Usage> usages = new ArrayList<>();
    private final String userId;

    public void logUsage(String description, OpenAiApi.Usage usage){
        Usage newUsage = new Usage();
        newUsage.setDescription(description);
        newUsage.setPromptTokens(usage.promptTokens());
        newUsage.setCompletionTokens(usage.completionTokens());
        newUsage.setModelVersion(OpenAiService.GENERATION_MODEL);
        usages.add(newUsage);
    }

    public Mono<Void> saveUsages(UsageRepository usageRepository,UUID adventureId, UUID adventureNodeId){
        for (Usage usage : usages) {
            usage.setAdventureId(adventureId);
            usage.setAdventureNodeId(adventureNodeId);
            usage.setUserId(userId);
        }
        return usageRepository.saveAll(usages).then();
    }

}
