package com.alexk8s.cyoaai.service;

import com.alexk8s.cyoaai.model.User;
import com.alexk8s.cyoaai.model.UserDailyUsage;
import com.alexk8s.cyoaai.repository.UserDailyUsageRepository;
import com.alexk8s.cyoaai.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.security.Principal;
import java.time.*;
import java.util.UUID;

import static java.time.temporal.ChronoUnit.DAYS;
import static java.time.temporal.ChronoUnit.SECONDS;

@Service
@AllArgsConstructor
@Slf4j
public class CreditsService {
    private final UserRepository userRepository;
    private final UserDailyUsageRepository userDailyUsageRepository;
    private static final int DEFAULT_CREDITS = 10;

    public record UserCredits(int total, int remaining, long resetsInSeconds, ZonedDateTime resetsAt) {
    }

    private Mono<Integer> getUsage(String userId, LocalDate localDate) {
        return userDailyUsageRepository.findByUserIdAndDate(userId, localDate)
                .map(UserDailyUsage::getUsage)
                .defaultIfEmpty(0);
    }

    private Mono<User> getOrCreateUser(String userId) {
        return userRepository.findById(userId)
                .switchIfEmpty(userRepository.save(new User(userId, DEFAULT_CREDITS)));
    }

    public Mono<UserCredits> getUserCredits(Principal principal) {
        String userId = principal.getName();
        return Mono.zip(getOrCreateUser(userId), getUsage(userId, LocalDate.now()), this::buildUserCredits);
    }

    private UserCredits buildUserCredits(User user, Integer used) {
        int total = user.getDailyCredits();
        int remaining = total - used;
        ZonedDateTime resetTime = ZonedDateTime.now(ZoneId.systemDefault()).plusDays(1).truncatedTo(DAYS);
        long resetSeconds = LocalDateTime.now().until(resetTime, SECONDS);
        return new UserCredits(total, remaining, resetSeconds, resetTime);
    }

    public Mono<Void> logAdventure(Principal principal, UUID adventureId) {
        String userId = principal.getName();
        Mono<UserDailyUsage> userDailyUsageMono = userDailyUsageRepository.findByUserIdAndDate(userId, LocalDate.now())
                .switchIfEmpty(Mono.just(new UserDailyUsage(userId, LocalDate.now())));
        return userDailyUsageMono
                .doOnNext(udu -> udu.getAdventures().add(adventureId))
                .flatMap(userDailyUsageRepository::save)
                .then();
    }

    public Mono<Void> logAdventureNode(Principal principal, UUID adventureNodeId) {
        String userId = principal.getName();
        Mono<UserDailyUsage> userDailyUsageMono = userDailyUsageRepository.findByUserIdAndDate(userId, LocalDate.now())
                .switchIfEmpty(Mono.just(new UserDailyUsage(userId, LocalDate.now())));
        return userDailyUsageMono
                .doOnNext(udu -> udu.getAdventureNodes().add(adventureNodeId))
                .flatMap(userDailyUsageRepository::save)
                .then();
    }

    public Mono<Void> logAdventureNodeTts(Principal principal, UUID adventureNodeId) {
        String userId = principal.getName();
        Mono<UserDailyUsage> userDailyUsageMono = userDailyUsageRepository.findByUserIdAndDate(userId, LocalDate.now())
                .switchIfEmpty(Mono.just(new UserDailyUsage(userId, LocalDate.now())));
        return userDailyUsageMono
                .doOnNext(udu -> udu.getTtses().add(adventureNodeId))
                .flatMap(userDailyUsageRepository::save)
                .then();
    }


}
