package com.alexk8s.cyoaai.service;

import com.alexk8s.cyoaai.configuration.S3ClientConfigurationProperties;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import software.amazon.awssdk.core.async.AsyncRequestBody;
import software.amazon.awssdk.core.async.AsyncResponseTransformer;
import software.amazon.awssdk.core.async.ResponsePublisher;
import software.amazon.awssdk.services.s3.S3AsyncClient;
import software.amazon.awssdk.services.s3.model.*;

import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.file.Path;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.BiFunction;
import java.util.function.Function;

@AllArgsConstructor
@Service
@Slf4j
public class ObjectStorageService {
    private final S3AsyncClient s3client;
    private final S3ClientConfigurationProperties s3ClientConfiguration;
    private final ExecutorService executorService = Executors.newSingleThreadExecutor();

    public Mono<PutObjectResponse> upload(String bucket, String key, MediaType mediaType, Path filePath) {
        return upload(bucket, key, mediaType, filePath, AsyncRequestBody::fromFile);
    }

    public Mono<PutObjectResponse> upload(String bucket, String key, MediaType mediaType, byte[] bytes) {
        return upload(bucket, key, mediaType, bytes, AsyncRequestBody::fromBytes);
    }

    public Mono<PutObjectResponse> upload(String bucket, String key, MediaType mediaType, InputStream inputStream,long length) {
        return upload(bucket, key, mediaType, inputStream, (i)->AsyncRequestBody.fromInputStream(i,length,executorService));
    }

    private <T> Mono<PutObjectResponse> upload(String bucket, String key, MediaType mediaType, T bodyContent, Function<T,AsyncRequestBody> bodyFunction){
        PutObjectRequest request = PutObjectRequest.builder()
                .bucket(bucket)
                .key(key)
                .contentType(mediaType.toString())
                .build();
        AsyncRequestBody body = bodyFunction.apply(bodyContent);
        CompletableFuture<PutObjectResponse> future = s3client.putObject(request, body);
        return upload(future,request);
    }


    private Mono<PutObjectResponse> upload(CompletableFuture<PutObjectResponse> future,PutObjectRequest request){
        return Mono.fromFuture(future)
                .doOnSuccess(r -> log.info("Uploaded {} to s3", request.key()))
                .doOnError(e -> log.error("Failed to upload {} to s3", request.key(), e));
    }

    public Flux<S3Object> listObjects(String bucket, String prefix) {
        ListObjectsV2Request request = ListObjectsV2Request.builder()
                .bucket(bucket)
                .prefix(prefix)
                .build();
        return Flux.from(s3client.listObjectsV2Paginator(request))
                .flatMapIterable(ListObjectsV2Response::contents);
    }

    public Mono<ResponsePublisher<GetObjectResponse>> getObject(String bucket, String key) {
        GetObjectRequest request = GetObjectRequest.builder()
                .bucket(bucket)
                .key(key)
                .build();
        return Mono.fromFuture(s3client.getObject(request, AsyncResponseTransformer.toPublisher()));
    }

    public Mono<Path> download(String bucket, String key, Path path) {
        return getObject(bucket, key)
                .flatMap(r -> saveObject(r, path))
                .thenReturn(path)
                .doOnTerminate(() -> log.info("Downloaded {} from object storage", key));
    }

    private Mono<Void> saveObject(ResponsePublisher<GetObjectResponse> responsePublisher, Path path) {
        Flux<DataBuffer> dataBufferFlux = Flux.from(responsePublisher).map(DefaultDataBufferFactory.sharedInstance::wrap);
        return DataBufferUtils.write(dataBufferFlux, path);

    }

    public String getAccessUrl(String bucket, String key) {
        return s3ClientConfiguration.getEndpoint() + "/" + bucket + "/" + key;
    }

}
