package com.alexk8s.cyoaai.service;

import com.alexk8s.cyoaai.model.Adventure;
import com.alexk8s.cyoaai.model.AdventureNode;
import com.alexk8s.cyoaai.repository.AdventureNodeRepository;
import com.alexk8s.cyoaai.repository.AdventureRepository;
import com.alexk8s.cyoaai.repository.UsageRepository;
import com.alexk8s.cyoaai.util.UsageContext;
import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.ai.openai.api.OpenAiApi;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.retry.Retry;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@AllArgsConstructor
@Slf4j
public class AdventureGenerationService {
    private static final float GENERATION_TEMPERATURE = 1.1f;
    private static final String METADATA_BASE_PROMPT =
            """
                    Write the following for a choose your own adventure prompt:
                    - A title for the adventure
                    - Tags for the adventure separated by whitespace
                    - The tone for the adventure
                        * Casual
                        * Serious
                        * Horror
                        * Mystery
                        * Other
                    - The setting for the adventure
                        * Fantasy
                        * Sci-fi
                        * Modern
                        * Historical
                        * Post-apocalyptic
                        * Other
                                
                                
                    Separate each of these with a newline. For example:
                        **Input:** Write a story about a dragon
                        **Output:**
                            The dragon's lair
                            fantasy adventure dragon
                            A serious tone
                            Fantasy setting
                    **Prompt:**
                    """;
    private static final String CHOOSE_YOUR_OWN_ADVENTURE_SYSTEM_PROMPT = "**Objective:**\n" +
            "Write a choose your own adventure story based on the provided prompt.\n\n" +
            "**Guidelines:**\n" +
            "- Provide very specific details. Details should be specific. Try and use many fictional proper nouns\n" +
            "- Do not repeat or summarize and recount the events that have already occured\n" +
            "- Allow choices to result in failures or have significant negative consequences \n" +
            "- write an coherent story that continues from the previous sections\n" +
            "- Provide 2-4 choices for the reader to make at the end of each section.\n" +
            "- Provide some choices that are negative or a bad idea or unexpected\n" +
            "- End the story with a question.\n" +
            "- Don't introduce magic or fantasy elements unless they were already present in the story\n" +
            "- Use * for bold and _ for italics, don't apply formatting to the choices\n" +
            "- The answers should not be prefixed with any numbering or bullets\n" +
            "- Each choice should be a single word or a phrase\n\n" +
            "**Output Format do in this order:**\n" +
            "1. Write the story\n" +
            "2. Write the question to the user\n" +
            "3. Add the separator '---' to divide the story and the choices\n" +
            "4. Write the choices the reader can make\n" +
            "**Example output to use this format:**\n" +
            "You are standing in front of a vault. The old man says \n_So you have made it to the vault_\n....(story etc...)\n*What would you like to do?*\n" +
            "---\n" +
            "Open the vault\n" +
            "Eat the key\n" +
            "Leave the vault\n";
    private static final String[] SUMMARY_EXAMPLES = new String[]{
            """
            {
              "key_events": [
                  "The player found a key",
                  "The player used the key to open a door"
              ],
              "choice_consequences": {
                "walk away": "The player misses out on a valuable item",
                "open the door": "The player finds a treasure chest",
                "examine the key": "The player discovers a hidden compartment"
              },
              "character_developments": {
                 "Mary": "Mary agrees to help the player",
                 "John": "John dies and leaves the story",
                 "Player": "The player gains the ability to see through doors"
                },
              "short_term_goal": "Find the treasure chest",
              "long_term_goal": "Defeat the evil wizard and save the village of Oakwood",
              "overcome_obstacles": ["The player must defeat the guardian of the treasure chest"],
              "hidden_information": "The treasure chest is booby-trapped",
              "setting": ["A dark cave", "Medieval fantasy", "Serious"],
              "historical_events": [
                "The player entered the village of Oakwood",
                "The player met Mary and John",
                "The evil wizard attacked the village",
                "The player tracked the wizard to the cave",
                "The player entered the cave"
              ]
            """,
            """
            {
              "key_events": [
                  "The player started a new job",
                  "Jod (CEO) told the player to go to the basement",
                  ],
              "choice_consequences": {
                  "go to the basement": "The player finds a secret room",
                  "leave the building": "The player gets fired",
                  "talk to the receptionist": "The player learns about the company's history",
                  "try to pick the lock": "The player gets caught by security"
              },
              "character_developments": {
                  "Jod": "Jod becomes more suspicious of the player"
              },
              "short_term_goal": "Find out what's in the basement",
              "long_term_goal": "Become the CEO of the company",
              "overcome_obstacles": [
                  "The player must find the key to the basement"
              ],
              "hidden_information": "The basement contains a portal to another dimension",
              "setting": ["A corporate office building", "Modern", "Serious"],
              "historical_events": [
                "The player met Jod"
              ]
            """
    };
    private static final String SUMMARY_PROMPT = """
            Generate a concise summary of the following game section in JSON format. Build on the existing context and historical events if available. Do not assume the previous context will be available in the next section.
            The summary should provide all necessary context to allow for a coherent and consistent continuation of the story in subsequent sections by setting the trajectory.
            The examples are not exhaustive, feel free to add more context as needed
            Fields:
              - key_events: The most important events that occurred in this section
              - choice_consequences: The consequences of the choices available made in this section
              - character_developments: Any changes to the characters in this section
              - short_term_goal: The immediate goal of the player
              - long_term_goal: The overarching goal of the player/story
              - overcome_obstacles: The obstacles the player must overcome
              - hidden_information: Any hidden information the player does not yet know
              - setting: The setting of the story
              - historical_events: The events that have occurred in the story so far (append to this list)
            Use the following examples as a guide:
            """ + String.join("\n\n", SUMMARY_EXAMPLES);

    private static final String PARSE_PROMPT = "You will be provided with a section of a choose your own adventure story. Parse the section in to two parts: content and choices and output into a json object. Trim any ordered or unordered list formatting from the choices. Maintain any newlines or formatting from the content, escape any necessary characters.\n" +
            "Example:\n" +
            """
                    {"content":"the content of the section","choices":["the first choice","the second choice"]}""";
    private final OpenAiService openAiService;
    private final AdventureRepository adventureRepository;
    private final AdventureNodeRepository adventureNodeRepository;
    private final UsageRepository usageRepository;
    private final ParentChainService parentChainService;
    private final ObjectMapper objectMapper = new ObjectMapper();

    public Mono<Adventure> generateAdventure(String adventurePrompt,String userId) {
        String prompt = METADATA_BASE_PROMPT + adventurePrompt;
        UsageContext usageContext = new UsageContext(userId);
        return openAiService.getResponse(prompt, GENERATION_TEMPERATURE)
                .doOnNext(logUsage("Adventure generation", usageContext))
                .map(o -> o.choices().getFirst().message().content().replaceAll("\n{2,}", "\n").trim().replaceAll("^\\**[Oo]utput:\\**", ""))
                .map(output -> {
                    Metadata metadata = parseMetadata(output);
                    Adventure adventure = new Adventure();
                    adventure.setTitle(metadata.title);
                    adventure.setTags(Arrays.asList(metadata.tags));
                    adventure.setPrompt(adventurePrompt);
                    adventure.setRawOutput(output);
                    return adventure;
                }).flatMap(adventureRepository::save)
                .delayUntil(a -> generateInitialNode(adventurePrompt, a, usageContext))
                .delayUntil(a -> usageContext.saveUsages(usageRepository, a.getId(), null));
    }

    private record Metadata(String title, String[] tags) {
    }

    private Metadata parseMetadata(String generatedText) {
        String[] lines = generatedText.trim().split("\n");
        if (lines.length < 2) {
            throw new IllegalArgumentException("Invalid metadata");
        }
        String title = lines[0].replaceAll("\\**[Tt]itle:\\**", "").trim();
        String[] tags = lines[1].replaceAll("\\**[Tt]ags?:\\**", "").replaceAll("\\s{2,}", "").split(" ");
        return new Metadata(title, tags);
    }

    private record RawNode(@JsonAlias("content") String text, List<String> choices) {
    }

    public Mono<AdventureNode> generateInitialNode(String prompt, Adventure adventure,String userId) {
        UsageContext usageContext = new UsageContext(userId);
        return generateInitialNode(prompt, adventure, usageContext)
                .delayUntil(n -> usageContext.saveUsages(usageRepository, adventure.getId(), n.getId()));
    }

    public Mono<AdventureNode> generateInitialNode(String prompt, Adventure adventure, UsageContext usageContext) {
        OpenAiApi.ChatCompletionMessage systemMessage = new OpenAiApi.ChatCompletionMessage(CHOOSE_YOUR_OWN_ADVENTURE_SYSTEM_PROMPT, OpenAiApi.ChatCompletionMessage.Role.SYSTEM);
        OpenAiApi.ChatCompletionMessage adventureContextMessage = new OpenAiApi.ChatCompletionMessage("Adventure Details:\n" + adventure.getRawOutput(), OpenAiApi.ChatCompletionMessage.Role.USER);
        OpenAiApi.ChatCompletionMessage userMessage = new OpenAiApi.ChatCompletionMessage(prompt, OpenAiApi.ChatCompletionMessage.Role.USER);
        List<OpenAiApi.ChatCompletionMessage> messages = List.of(systemMessage, adventureContextMessage, userMessage);
        return openAiService.getResponse(messages, GENERATION_TEMPERATURE)
                .doOnNext(logUsage("Initial node generation", usageContext))
                .zipWhen(x -> parseNode(x, usageContext))
                .flatMap(output -> {
                    String outputContent = output.getT1().choices().getFirst().message().content();
                    RawNode rawNode = output.getT2();
                    if (rawNode.choices.size() < 2) {
                        log.warn("Invalid node, {} choices found, expected at least 2.\n{}", rawNode.choices.size(), outputContent);
                        return Mono.error(new AdventureGenerationService.InvalidNodeException("Invalid node, " + rawNode.choices.size() + " choices found, expected at least 2."));
                    }
                    log.info("Generated node: {}", outputContent);
                    AdventureNode node = new AdventureNode();
                    node.setAdventureId(adventure.getId());
                    node.setRawOutput(outputContent);
                    return populateNode(node, messages, rawNode, usageContext);
                }).retryWhen(Retry.max(4).filter(e -> e instanceof AdventureGenerationService.InvalidNodeException));
    }

    private Mono<RawNode> parseNode(OpenAiApi.ChatCompletion chatCompletion, UsageContext usageContext) {
        String content = chatCompletion.choices().getFirst().message().content();
        List<OpenAiApi.ChatCompletionMessage> messages = List.of(
                new OpenAiApi.ChatCompletionMessage(PARSE_PROMPT, OpenAiApi.ChatCompletionMessage.Role.SYSTEM),
                new OpenAiApi.ChatCompletionMessage(content, OpenAiApi.ChatCompletionMessage.Role.USER)
        );
        return openAiService.getResponse(messages, 1.0f)
                .doOnNext(logUsage("Json conversions", usageContext))
                .map(output -> {
                    String outputContent = output.choices().getFirst().message().content();
                    return parseNodeJson(outputContent);
                }).retryWhen(Retry.max(5).filter(e -> e instanceof InvalidNodeException));
    }

    private Consumer<OpenAiApi.ChatCompletion> logUsage(String description, UsageContext usageContext) {
        return chatCompletion -> {
            usageContext.logUsage(description, chatCompletion.usage());
            log.info("{} consumed {} tokens", description, chatCompletion.usage().totalTokens());
        };
    }

    public static class InvalidNodeException extends RuntimeException {
        public InvalidNodeException(String message) {
            super(message);
        }
    }

    private Mono<AdventureNode> populateNode(AdventureNode node, List<OpenAiApi.ChatCompletionMessage> messages, RawNode rawNode, UsageContext usageContext) {
        String fullPrompt = messages.stream().map(OpenAiApi.ChatCompletionMessage::content).collect(Collectors.joining("\n\n\n"));
        node.setPrompt(fullPrompt);
        node.setGenerated(true);
        node.setContent(rawNode.text);
        return generateSummary(node, usageContext)
                .flatMap(adventureNodeRepository::save)
                .delayUntil(n -> createChoices(n, rawNode.choices));

    }

    private Mono<AdventureNode> generateSummary(AdventureNode adventureNode, UsageContext usageContext) {
        Mono<String> summaryMono;
        if (adventureNode.getParentId() == null)
            summaryMono = generateSummary(null, null, adventureNode.getContent(), usageContext);
        else
            summaryMono = adventureNodeRepository.findById(adventureNode.getParentId())
                    .flatMap(parent -> generateSummary(parent.getSummary(), adventureNode.getTitle(), adventureNode.getContent(), usageContext));
        return summaryMono.map(s -> {
            adventureNode.setSummary(s);
            return adventureNode;
        });
    }

    private Mono<String> generateSummary(String previousSummary, String choice, String content, UsageContext usageContext) {
        OpenAiApi.ChatCompletionMessage systemMessage = new OpenAiApi.ChatCompletionMessage(SUMMARY_PROMPT, OpenAiApi.ChatCompletionMessage.Role.SYSTEM);
        OpenAiApi.ChatCompletionMessage userMessage = new OpenAiApi.ChatCompletionMessage(content, OpenAiApi.ChatCompletionMessage.Role.USER);
        List<OpenAiApi.ChatCompletionMessage> messages;
        if (previousSummary != null) {
            OpenAiApi.ChatCompletionMessage previousSummaryMessage = new OpenAiApi.ChatCompletionMessage("Context:\n" + previousSummary, OpenAiApi.ChatCompletionMessage.Role.USER);
            OpenAiApi.ChatCompletionMessage choiceMessage = new OpenAiApi.ChatCompletionMessage("Choice:\n" + choice, OpenAiApi.ChatCompletionMessage.Role.USER);
            messages = List.of(systemMessage, previousSummaryMessage, choiceMessage, userMessage);
        } else {
            messages = List.of(systemMessage, userMessage);
        }
        return openAiService.getResponse(messages, 1.0f)
                .doOnNext(logUsage("Summary generation", usageContext))
                .map(output -> output.choices().getFirst().message().content().replaceAll("```(json)?", ""));
    }

    private Flux<AdventureNode> createChoices(AdventureNode parent, List<String> choices) {
        return Flux.fromIterable(choices)
                .map(choice -> {
                    AdventureNode node = new AdventureNode();
                    node.setParentId(parent.getId());
                    node.setDepth(parent.getDepth() + 1);
                    node.setAdventureId(parent.getAdventureId());
                    node.setTitle(choice);
                    return node;
                }).flatMap(adventureNodeRepository::save);
    }

    private RawNode parseNodeJson(String jsonText) {
        String cleanJson = jsonText.replaceAll("```(json)?", "");
        try {
            return objectMapper.readValue(cleanJson, RawNode.class);
        } catch (JsonProcessingException e) {
            log.error("Failed to parse node json: {}", cleanJson, e);
            throw new InvalidNodeException("Failed to parse node json: " + e.getMessage());
        }
    }

    public Mono<AdventureNode> populateNode(UUID nodeId, String guidance,String userId) {
        UsageContext usageContext = new UsageContext(userId);
        Mono<AdventureNode> node = adventureNodeRepository.findById(nodeId).share();
        Mono<List<AdventureNode>> ancestors = getAncestors(nodeId, 2);
        Mono<Adventure> adventureMono = node.flatMap(n -> adventureRepository.findById(n.getAdventureId()));
        return Mono.zip(node, ancestors, adventureMono)
                .flatMap(t -> populateNode(t.getT1(), t.getT2(), t.getT3(), guidance, usageContext))
                .delayUntil(n -> usageContext.saveUsages(usageRepository, n.getAdventureId(), n.getId()));
    }

    private Mono<List<AdventureNode>> getAncestors(UUID nodeId, int count) {
        return parentChainService.getChain(nodeId)
                .map(c -> c.subList(Math.max(0, c.size() - count), c.size()))
                .flatMapMany(adventureNodeRepository::findAllById)
                .collectList();
    }

    private Mono<AdventureNode> populateNode(AdventureNode node, List<AdventureNode> ancestors, Adventure adventure, String guidance, UsageContext usageContext) {
        OpenAiApi.ChatCompletionMessage systemMessage = new OpenAiApi.ChatCompletionMessage(CHOOSE_YOUR_OWN_ADVENTURE_SYSTEM_PROMPT, OpenAiApi.ChatCompletionMessage.Role.SYSTEM);
        OpenAiApi.ChatCompletionMessage adventureContextMessage = new OpenAiApi.ChatCompletionMessage("Adventure Details:\n" + adventure.getRawOutput(), OpenAiApi.ChatCompletionMessage.Role.USER);
        OpenAiApi.ChatCompletionMessage userMessage = new OpenAiApi.ChatCompletionMessage(node.getTitle(), OpenAiApi.ChatCompletionMessage.Role.USER);
        List<OpenAiApi.ChatCompletionMessage> messages = new ArrayList<>(List.of(systemMessage, adventureContextMessage));
        messages.addAll(getContextMessages(ancestors));
        if (guidance != null) {
            OpenAiApi.ChatCompletionMessage guidanceMessage = new OpenAiApi.ChatCompletionMessage(guidance, OpenAiApi.ChatCompletionMessage.Role.USER);
            messages.add(guidanceMessage);
        }
        messages.add(userMessage);
        return openAiService.getResponse(messages, GENERATION_TEMPERATURE)
                .doOnNext(logUsage("Node generation", usageContext))
                .zipWhen(x -> parseNode(x, usageContext))
                .flatMap(output -> {
                    String outputContent = output.getT1().choices().getFirst().message().content();
                    RawNode rawNode = output.getT2();
                    node.setRawOutput(outputContent);
                    node.setGuidance(guidance);
                    return populateNode(node, messages, rawNode, usageContext);
                }).retryWhen(Retry.max(4).filter(e -> e instanceof AdventureGenerationService.InvalidNodeException));
    }

    private List<OpenAiApi.ChatCompletionMessage> getContextMessages(List<AdventureNode> ancestors) {
        if (ancestors.isEmpty())
            return Collections.emptyList();
        String context = "**Story Context (* denotes a reader choice)**:" + ancestors.stream()
                .flatMap(a -> Stream.of("*" + a.getTitle(), a.getSummary()))
                .filter(Objects::nonNull)
                .collect(Collectors.joining("\n"));
        OpenAiApi.ChatCompletionMessage contextMessage = new OpenAiApi.ChatCompletionMessage(context, OpenAiApi.ChatCompletionMessage.Role.SYSTEM);
        return List.of(contextMessage);
    }
}
