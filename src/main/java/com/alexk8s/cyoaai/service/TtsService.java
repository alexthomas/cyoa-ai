package com.alexk8s.cyoaai.service;

import com.alexk8s.cyoaai.model.Adventure;
import com.alexk8s.cyoaai.model.AdventureNode;
import com.alexk8s.cyoaai.model.Usage;
import com.alexk8s.cyoaai.repository.AdventureNodeRepository;
import com.alexk8s.cyoaai.repository.AdventureRepository;
import com.alexk8s.cyoaai.repository.UsageRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.ai.openai.api.OpenAiAudioApi;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.core.io.buffer.DefaultDataBuffer;
import org.springframework.core.io.buffer.DefaultDataBufferFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;
import software.amazon.awssdk.services.s3.model.PutObjectResponse;

import java.io.InputStream;
import java.nio.ByteBuffer;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TtsService {
    private static final String TTS_BUCKET = "tts";
    private static final List<OpenAiAudioApi.SpeechRequest.Voice> ADVENTURE_VOICES = List.of(OpenAiAudioApi.SpeechRequest.Voice.FABLE, OpenAiAudioApi.SpeechRequest.Voice.ONYX, OpenAiAudioApi.SpeechRequest.Voice.NOVA);
    private static final MediaType AUDIO_MEDIA_TYPE = new MediaType("audio", "mpeg");

    private final AdventureNodeRepository adventureNodeRepository;
    private final AdventureRepository adventureRepository;
    private final OpenAiService openAiService;
    private final ObjectStorageService objectStorageService;
    private final UsageRepository usageRepository;

    public Flux<ByteBuffer> generateTts(AdventureNode node, String userId) {
        return adventureRepository.findById(node.getAdventureId())
                .flatMap(a -> {
                    if (a.getTtsVoice() == null) {
                        a.setTtsVoice(ADVENTURE_VOICES.get((int) (Math.random() * ADVENTURE_VOICES.size())).toString());
                        return adventureRepository.save(a);
                    }
                    return Mono.just(a);
                })
                .flatMapMany(a -> generateTts(a, node, userId));
    }

    private Flux<ByteBuffer> generateTts(Adventure adventure, AdventureNode adventureNode, String userId) {
        String key = adventureNode.getId().toString() + ".mp3";
        Flux<ByteBuffer> speech = openAiService.getSpeech(adventureNode.getContent(), OpenAiAudioApi.SpeechRequest.Voice.valueOf(adventure.getTtsVoice()))
                .replay(Duration.ofHours(1)).autoConnect();
        String accessUrl = objectStorageService.getAccessUrl(TTS_BUCKET, key);
        List<ByteBuffer> byteBuffers = new ArrayList<>();
        Mono<Void> upload = Mono.defer(() -> uploadFile(key, byteBuffers))
                .delayUntil(r -> {
                    adventureNode.setTtsUrl(accessUrl);
                    return adventureNodeRepository.save(adventureNode);
                }).delayUntil(n -> logUsage(adventureNode, userId)).then();
        return speech
                .doOnNext(byteBuffers::add)
                .publishOn(Schedulers.boundedElastic())
                .doOnComplete(upload::block);
    }

    private Mono<PutObjectResponse> uploadFile(String key, List<ByteBuffer> byteBuffers) {
        Flux<DefaultDataBuffer> dataBuffers = Flux.fromIterable(byteBuffers).map(DefaultDataBufferFactory.sharedInstance::wrap);
        Mono<DataBuffer> dataBufferMono = DataBufferUtils.join(dataBuffers);
        return dataBufferMono.flatMap(d -> {
            int length = d.readableByteCount();
            InputStream inputStream = d.asInputStream();
            return objectStorageService.upload(TTS_BUCKET, key, AUDIO_MEDIA_TYPE, inputStream, length);
        });

    }

    private Mono<Usage> logUsage(AdventureNode adventureNode, String userId) {
        Usage usage = new Usage();
        usage.setUserId(userId);
        usage.setAdventureNodeId(adventureNode.getId());
        usage.setAdventureId(adventureNode.getAdventureId());
        usage.setDescription("Text to Speech");
        usage.setModelVersion(OpenAiService.TTS_MODEL);
        usage.setPromptTokens(adventureNode.getContent().length());
        return usageRepository.save(usage);
    }
}
