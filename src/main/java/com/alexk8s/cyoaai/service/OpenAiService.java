package com.alexk8s.cyoaai.service;


import lombok.extern.slf4j.Slf4j;
import org.springframework.ai.openai.api.OpenAiApi;
import org.springframework.ai.openai.api.OpenAiAudioApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.nio.ByteBuffer;
import java.util.List;
import java.util.Objects;

@Service
@Slf4j
public class OpenAiService {

    private static final String OPENAI_BASE_URL = "https://api.openai.com";
    private static final String CHAT_URL = "/v1/chat/completions";
    public static final String GENERATION_MODEL = "gpt-4o";
    public static final String TTS_MODEL = OpenAiAudioApi.TtsModel.TTS_1.getValue();

    private final WebClient webClient;

    @Autowired
    public OpenAiService(@Value("${SPRING_AI_OPENAI_API_KEY}") String openAiApiKey) {
        this.webClient = WebClient.builder()
                .baseUrl(OPENAI_BASE_URL)
                .defaultHeader("Authorization", "Bearer " + openAiApiKey)
                .build();
    }



    public Mono<OpenAiApi.ChatCompletion> getResponse(String prompt, float temperature) {
        OpenAiApi.ChatCompletionMessage chatCompletionMessage = new OpenAiApi.ChatCompletionMessage(prompt, OpenAiApi.ChatCompletionMessage.Role.SYSTEM);
        return getResponse(List.of(chatCompletionMessage),temperature);
    }


    public Mono<OpenAiApi.ChatCompletion> getResponse(List<OpenAiApi.ChatCompletionMessage> messages,float temperature) {
        OpenAiApi.ChatCompletionRequest chatCompletionRequest = new OpenAiApi.ChatCompletionRequest(messages, GENERATION_MODEL, temperature);
        return webClient.post()
                .uri(CHAT_URL)
                .bodyValue(chatCompletionRequest)
                .retrieve()
                .onStatus(HttpStatusCode::isError, clientResponse -> clientResponse.bodyToMono(String.class)
                        .doOnNext(s->log.error("Error getting response: {}",s)).map(RuntimeException::new))
                .toEntity(OpenAiApi.ChatCompletion.class)
                .mapNotNull(r->{
                    logRateLimit(r);
                    return r.getBody();
                });
    }

    private void logRateLimit(ResponseEntity<?> responseEntity){
        if(Objects.nonNull(responseEntity.getHeaders().get("x-ratelimit-remaining-tokens"))){
            log.info("Remaining rate limit: {}, reset in {}", responseEntity.getHeaders().get("x-ratelimit-remaining-tokens"), responseEntity.getHeaders().get("x-ratelimit-reset-tokens"));
        }
    }

    public Flux<ByteBuffer> getSpeech(String text, OpenAiAudioApi.SpeechRequest.Voice voice){
        log.info("Getting speech for: {}",text);
        OpenAiAudioApi.SpeechRequest speechRequest = new OpenAiAudioApi.SpeechRequest(TTS_MODEL,text, voice, OpenAiAudioApi.SpeechRequest.AudioResponseFormat.MP3,1.0f);
        return webClient.post()
                .uri("/v1/audio/speech")
                .bodyValue(speechRequest)
                .retrieve()
                .onStatus(HttpStatusCode::isError, clientResponse -> clientResponse.bodyToMono(String.class)
                        .doOnNext(s->log.error("Error getting speech: {}",s)).map(RuntimeException::new))
                .bodyToFlux(ByteBuffer.class);
    }
}
