package com.alexk8s.cyoaai.service;

import com.alexk8s.cyoaai.model.ParentChain;
import com.alexk8s.cyoaai.repository.AdventureNodeRepository;
import com.alexk8s.cyoaai.repository.ParentChainRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

@Service
@AllArgsConstructor
@Slf4j
public class ParentChainService {
    private final ParentChainRepository parentChainRepository;
    private final AdventureNodeRepository adventureNodeRepository;

    public Mono<List<UUID>> getChain(UUID id) {
        return parentChainRepository.findById(id)
                .switchIfEmpty(generateChain(id))
                .map(ParentChain::getChain);
    }

    private Mono<ParentChain> generateChain(UUID id) {
        ParentChain parentChain = new ParentChain();
        parentChain.setId(id);
        Mono<List<UUID>> chainMono = adventureNodeRepository.findById(id)
                .flatMap(n -> {
                    if (n.getParentId() == null)
                        return Mono.just(Collections.emptyList());
                    return getChain(n.getParentId()).map(p -> append(p, n.getParentId()));
                });
        return chainMono.map(chain -> {
            parentChain.setChain(chain);
            return parentChain;
        }).flatMap(parentChainRepository::save)
                .onErrorResume(DuplicateKeyException.class, e -> parentChainRepository.findById(id));
    }

    private List<UUID> append(List<UUID> chain, UUID id) {
        List<UUID> mutableChain = new ArrayList<>(chain);
        mutableChain.add(id);
        return mutableChain;
    }
}
