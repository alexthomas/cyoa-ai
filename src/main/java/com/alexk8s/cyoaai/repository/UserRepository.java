package com.alexk8s.cyoaai.repository;

import com.alexk8s.cyoaai.model.User;
import org.springframework.data.r2dbc.repository.R2dbcRepository;

public interface UserRepository extends R2dbcRepository<User, String>{
}
