package com.alexk8s.cyoaai.repository;

import com.alexk8s.cyoaai.model.Usage;
import org.springframework.data.r2dbc.repository.R2dbcRepository;

import java.util.UUID;

public interface UsageRepository extends R2dbcRepository<Usage, UUID> {
}
