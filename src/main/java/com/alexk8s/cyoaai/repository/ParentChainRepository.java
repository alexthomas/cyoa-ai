package com.alexk8s.cyoaai.repository;

import com.alexk8s.cyoaai.model.ParentChain;
import org.springframework.data.r2dbc.repository.R2dbcRepository;

import java.util.UUID;

public interface ParentChainRepository extends R2dbcRepository<ParentChain, UUID> {
}
