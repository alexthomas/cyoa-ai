package com.alexk8s.cyoaai.repository;

import com.alexk8s.cyoaai.model.Adventure;
import org.springframework.data.r2dbc.repository.R2dbcRepository;

import java.util.UUID;

public interface AdventureRepository extends R2dbcRepository<Adventure, UUID> {
}
