package com.alexk8s.cyoaai.repository;

import com.alexk8s.cyoaai.model.UserDailyUsage;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import reactor.core.publisher.Mono;

import java.time.LocalDate;

public interface UserDailyUsageRepository extends R2dbcRepository<UserDailyUsage, String> {
    Mono<UserDailyUsage> findByUserIdAndDate(String userId, LocalDate date);
}
