package com.alexk8s.cyoaai.repository;

import com.alexk8s.cyoaai.model.AdventureNode;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

public interface AdventureNodeRepository extends R2dbcRepository<AdventureNode, UUID> {
    Flux<AdventureNode> findByAdventureId(UUID adventureId);
    Flux<AdventureNode> findByParentId(UUID parentId);
    Flux<AdventureNode> findByAdventureIdAndParentId(UUID adventureId, UUID parentId);
    @Query("SELECT * FROM adventure_node WHERE id = (SELECT parent_id FROM adventure_node WHERE id = :id)")
    Mono<AdventureNode> findParent(UUID id);
}
