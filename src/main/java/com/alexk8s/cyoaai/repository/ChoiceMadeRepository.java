package com.alexk8s.cyoaai.repository;

import com.alexk8s.cyoaai.model.ChoiceMade;
import org.springframework.data.r2dbc.repository.R2dbcRepository;

import java.util.UUID;

public interface ChoiceMadeRepository extends R2dbcRepository<ChoiceMade, UUID> {
}
