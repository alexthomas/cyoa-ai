package com.alexk8s.cyoaai.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import software.amazon.awssdk.regions.Region;

import java.net.URI;

@ConfigurationProperties("s3")
@Data
public class S3ClientConfigurationProperties {
    private URI endpoint;
    private Region region;
    private String accessKeyId;
    private String secretAccessKey;
}
