package com.alexk8s.cyoaai.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@EqualsAndHashCode(callSuper = true)
@Table("user_daily_usage")
@NoArgsConstructor
public class UserDailyUsage extends StandardEntity {
    private String userId;
    private LocalDate date;
    private List<UUID> adventures;
    private List<UUID> adventureNodes;
    private List<UUID> ttses;

    public UserDailyUsage(String userId, LocalDate date) {
        this.userId = userId;
        this.date = date;
        adventureNodes=new ArrayList<>();
        adventures=new ArrayList<>();
        ttses=new ArrayList<>();
    }

    @Transient
    public int getUsage() {
        int usage = 0;
        if (adventures != null)
            usage += adventures.size() * 2;
        if (adventureNodes != null)
            usage += adventureNodes.size();
        if (ttses != null)
            usage += ttses.size();
        return usage;
    }
}
