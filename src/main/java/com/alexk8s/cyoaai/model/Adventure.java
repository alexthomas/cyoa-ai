package com.alexk8s.cyoaai.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.relational.core.mapping.Table;

import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@Table("adventure")
public class Adventure extends StandardEntity{
    private String title;
    private List<String> tags;
    private String ttsVoice;
    @JsonIgnore
    private String rawOutput;
    @JsonIgnore
    private String prompt;
    @CreatedBy
    @JsonIgnore
    private String createdBy;

}
