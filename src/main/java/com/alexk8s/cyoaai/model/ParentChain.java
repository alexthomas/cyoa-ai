package com.alexk8s.cyoaai.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.relational.core.mapping.Table;

import java.util.List;
import java.util.UUID;

@Data
@EqualsAndHashCode(callSuper = true)
@Table("parent_chain")
public class ParentChain extends StandardEntity {
    private List<UUID> chain;
}
