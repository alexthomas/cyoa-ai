package com.alexk8s.cyoaai.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.relational.core.mapping.Table;

import java.util.UUID;

@Data
@Table("usage")
@EqualsAndHashCode(callSuper = true)
public class Usage extends StandardEntity{
    private UUID adventureId;
    private UUID adventureNodeId;
    private String description;
    private int promptTokens;
    private int completionTokens;
    private String userId;
    private String modelVersion;
}
