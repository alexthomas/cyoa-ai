package com.alexk8s.cyoaai.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.relational.core.mapping.Table;

import java.util.UUID;

@Data
@EqualsAndHashCode(callSuper = true)
@Table("adventure_node")
public class AdventureNode extends StandardEntity{
    private UUID adventureId;
    private UUID parentId;
    private String title;
    private String content;
    private String summary;
    private Boolean generated;
    private Boolean userCreated;
    private int depth;
    @JsonIgnore
    private String prompt;
    @JsonIgnore
    private String rawOutput;
    private String guidance;
    private String ttsUrl;
    @CreatedBy
    @JsonIgnore
    private String createdBy;

}
