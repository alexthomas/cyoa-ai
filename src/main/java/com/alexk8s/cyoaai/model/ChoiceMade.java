package com.alexk8s.cyoaai.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.relational.core.mapping.Table;

import java.util.UUID;

@Data
@EqualsAndHashCode(callSuper = true)
@Table("choice_made")
public class ChoiceMade extends StandardEntity{
    private UUID choice;
}
