package com.alexk8s.cyoaai.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.Version;
import org.springframework.data.relational.core.mapping.Table;

import java.time.LocalDateTime;

@Data
@Table("\"user\"")
@NoArgsConstructor
public class User{

    @Id
    private String id;
    private int dailyCredits;
    @CreatedDate
    @JsonIgnore
    private LocalDateTime createdAt;
    @LastModifiedDate
    @JsonIgnore
    private LocalDateTime updatedAt;
    @Version
    @JsonIgnore
    private Long version;

    public User(String userId, int dailyCredits){
        this.id = userId;
        this.dailyCredits = dailyCredits;
    }
}
