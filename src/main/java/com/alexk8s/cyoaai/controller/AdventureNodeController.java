package com.alexk8s.cyoaai.controller;

import com.alexk8s.cyoaai.model.Adventure;
import com.alexk8s.cyoaai.model.AdventureNode;
import com.alexk8s.cyoaai.model.ChoiceMade;
import com.alexk8s.cyoaai.repository.AdventureNodeRepository;
import com.alexk8s.cyoaai.repository.AdventureRepository;
import com.alexk8s.cyoaai.repository.ChoiceMadeRepository;
import com.alexk8s.cyoaai.service.*;
import lombok.AllArgsConstructor;
import org.springframework.ai.openai.api.OpenAiAudioApi;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.nio.ByteBuffer;
import java.security.Principal;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@RestController
@RequestMapping("v1/adventure-node")
@AllArgsConstructor
@CrossOrigin
public class AdventureNodeController {


    private final AdventureNodeRepository adventureNodeRepository;
    private final AdventureGenerationService adventureGenerationService;
    private final ChoiceMadeRepository choiceMadeRepository;
    private final ParentChainService parentChainService;
    private final AdventureRepository adventureRepository;
    private final CreditsService creditsService;
    private final TtsService ttsService;
    private final Map<UUID, Flux<ByteBuffer>> cachedGeneratedTts = new ConcurrentHashMap<>();

    public record AddNodeRequest(UUID adventureId, UUID parentId, String title) {
    }

    @PostMapping
    public Mono<AdventureNode> addNode(@RequestBody AddNodeRequest request, Principal principal) {
        AdventureNode adventureNode = new AdventureNode();
        adventureNode.setAdventureId(request.adventureId);
        adventureNode.setParentId(request.parentId);
        adventureNode.setTitle(request.title);
        adventureNode.setUserCreated(true);
        return adventureNodeRepository.findById(request.parentId)
                .map(a -> {
                    adventureNode.setDepth(a.getDepth() + 1);
                    return adventureNode;
                })
                .flatMap(adventureNodeRepository::save)
                .delayUntil(n -> creditsService.logAdventureNode(principal, n.getAdventureId()))
                ;
    }

    @GetMapping("adventure/{adventureId}")
    public Flux<AdventureNode> getAdventureNodes(@PathVariable UUID adventureId) {
        return adventureNodeRepository.findByAdventureId(adventureId);
    }

    @GetMapping("{id}")
    public Mono<AdventureNode> getNode(@PathVariable UUID id) {
        return adventureNodeRepository.findById(id);
    }


    @GetMapping("adventure/{adventureId}/start")
    public Mono<AdventureNode> getAdventureStart(@PathVariable UUID adventureId) {
        return adventureNodeRepository.findByAdventureIdAndParentId(adventureId, null).next();
    }

    @GetMapping("{id}/choices")
    public Flux<AdventureNode> getChoices(@PathVariable UUID id) {
        return adventureNodeRepository.findByParentId(id);
    }

    public record GenerateRequest(String guidance) {
    }

    @PostMapping("{id}/generate")
    public Mono<AdventureNode> generate(@PathVariable UUID id, @RequestBody GenerateRequest request, Principal principal) {
        return adventureNodeRepository.findById(id)
                .flatMap(a -> {
                    if (a.getGenerated())
                        return Mono.just(a);
                    return adventureGenerationService.populateNode(a.getId(), request.guidance, principal.getName());
                })
                .delayUntil(n -> creditsService.logAdventureNode(principal, n.getAdventureId()));
    }

    @PostMapping("{id}/regenerate")
    public Mono<AdventureNode> regenerate(@PathVariable UUID id, Principal principal) {
        return adventureNodeRepository.findByParentId(id)
                .flatMap(adventureNodeRepository::delete)
                .then(adventureNodeRepository.findById(id))
                .flatMap(n -> {
                    if (n.getParentId() == null)
                        return adventureRepository.findById(n.getAdventureId())
                                .flatMap(a -> adventureGenerationService.generateInitialNode(a.getPrompt(), a, principal.getName()))
                                .delayUntil(n2 -> adventureNodeRepository.deleteById(id));
                    return adventureGenerationService.populateNode(n.getId(), n.getGuidance(), principal.getName());
                })
                .delayUntil(n -> creditsService.logAdventureNode(principal, n.getAdventureId()));
    }

    @PostMapping("{id}/choice")
    public Mono<ResponseEntity<Void>> makeChoice(@PathVariable UUID id) {
        ChoiceMade choiceMade = new ChoiceMade();
        choiceMade.setChoice(id);
        return choiceMadeRepository.save(choiceMade)
                .thenReturn(ResponseEntity.accepted().build());
    }

    @GetMapping("{id}/chain")
    public Mono<List<UUID>> getChain(@PathVariable UUID id) {
        return parentChainService.getChain(id);
    }

    public record TtsUrl(String url) {
    }

    @GetMapping("{id}/tts")
    public Mono<ResponseEntity<TtsUrl>> getAudio(@PathVariable UUID id, Principal principal, @RequestHeader("Host") String host) {
        return adventureNodeRepository.findById(id)
                .map(a -> {
                    String url;
                    if (a.getTtsUrl() == null) {
                        cachedGeneratedTts.put(id, ttsService.generateTts(a, principal.getName()).replay().autoConnect());
                        url = "http://"+host+"/v1/adventure-node/" + id + ".mp3"; //add http protocol because we don't know what the original protocol is, we'll assume if it should be https that the reverse proxy will handle it
                    } else {
                        url = a.getTtsUrl();
                    }
                    return ResponseEntity.ok(new TtsUrl(url));
                })
                .delayUntil(n -> creditsService.logAdventureNodeTts(principal, id));
    }

    @GetMapping(value = "{id}.mp3", produces = "audio/mpeg")
    public Flux<ByteBuffer> getAudioStream(@PathVariable UUID id) {
        return cachedGeneratedTts.get(id);
//                .doOnComplete(()->cachedGeneratedTts.remove(id));
    }


}
