package com.alexk8s.cyoaai.controller;

import com.alexk8s.cyoaai.model.Adventure;
import com.alexk8s.cyoaai.model.AdventureNode;
import com.alexk8s.cyoaai.repository.AdventureNodeRepository;
import com.alexk8s.cyoaai.repository.AdventureRepository;
import com.alexk8s.cyoaai.service.AdventureGenerationService;
import com.alexk8s.cyoaai.service.CreditsService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.security.Principal;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@RestController
@RequestMapping("v1/adventure")
@AllArgsConstructor
@CrossOrigin
public class AdventureController {

    private final AdventureRepository adventureRepository;
    private final AdventureGenerationService adventureGenerationService;
    private final CreditsService creditsService;
    private final AdventureNodeRepository adventureNodeRepository;

    @GetMapping
    public Flux<Adventure> getAdventures() {
        return adventureRepository.findAll();
    }

    @GetMapping("{id}")
    public Mono<Adventure> getAdventureById(@PathVariable UUID id) {
        return adventureRepository.findById(id);
    }

    @GetMapping(value = "{id}.txt",produces = "text/plain")
    public Mono<String> exportAdventureAsText(@PathVariable UUID id) {
        return Mono.zip(adventureRepository.findById(id), adventureNodeRepository.findByAdventureId(id).collectList(), this::exportAdventure);
    }

    private String exportAdventure(Adventure adventure, List<AdventureNode> nodes) {
        AdventureNode root = nodes.stream().filter(n -> n.getParentId() == null).findFirst().get();
        Map<UUID, List<AdventureNode>> nodesByParent = nodes.stream().filter(n -> n.getParentId() != null).collect(Collectors.groupingBy(AdventureNode::getParentId));
        return exportNode(adventure.getTitle(), root.getId(), nodesByParent, "");
    }

    private static final String PREFIX_INCREMENT = "    ";
    private static final String MID_BRANCH = "├── ";
    private static final String END_BRANCH = "└── ";

    private String exportNode(String title, UUID id, Map<UUID, List<AdventureNode>> nodesByParent, String prefix) {
        StringBuilder sb = new StringBuilder();
        sb.append(title);
        sb.append("\n ");
        List<AdventureNode> children = nodesByParent.get(id);
        if (children != null) {
            for (int i = 0; i < children.size(); i++) {
                AdventureNode child = children.get(i);
                boolean isLast = i == children.size() - 1;
                String nodePrefix = prefix+ (isLast ? END_BRANCH : MID_BRANCH);
                sb.append(nodePrefix);
                String childPrefix = prefix+(isLast ? " " : "│") + PREFIX_INCREMENT ;
                sb.append(exportNode(child.getTitle(), child.getId(), nodesByParent, childPrefix));
            }
        }
        return sb.toString();
    }

    public record AdventureRequest(String prompt) {
    }

    @PostMapping()
    public Mono<Adventure> createAdventure(@RequestBody AdventureRequest request, Principal principal) {
        return adventureGenerationService.generateAdventure(request.prompt, principal.getName())
                .delayUntil(a -> creditsService.logAdventure(principal, a.getId()));
    }
}
