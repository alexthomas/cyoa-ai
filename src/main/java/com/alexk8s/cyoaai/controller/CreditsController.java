package com.alexk8s.cyoaai.controller;

import com.alexk8s.cyoaai.service.CreditsService;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.security.Principal;

@RestController
@RequestMapping("v1/credits")
@AllArgsConstructor
@CrossOrigin
public class CreditsController {
    private final CreditsService creditsService;

    @GetMapping
    @PreAuthorize("isAuthenticated()")
    public Mono<CreditsService.UserCredits> getUserCredits(Principal principal) {
        return creditsService.getUserCredits(principal);
    }
}
