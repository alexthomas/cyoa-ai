package com.alexk8s.cyoaai.controller;

import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationToken;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.security.Principal;


@RestController
@RequestMapping("v1/user")
@CrossOrigin
public class UserController {

    @GetMapping
    public Mono<?> user(Principal principal) {
        if(principal instanceof JwtAuthenticationToken)
            return Mono.just(((JwtAuthenticationToken) principal).getTokenAttributes());
        return Mono.just(principal);
    }

}
